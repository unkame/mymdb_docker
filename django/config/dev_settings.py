"""
Configuration settings for development
"""

from config.common_settings import *

DEBUG = True
SECRET_KEY = '4&5&varl@0v=*4n11i7y@ajf@hc1_)rw94pm9ki&rboyk$ix7^'

INSTALLED_APPS += [
    # 3rd party
    'debug_toolbar',
]

MIDDLEWARE += [
	# 3rd party
    'debug_toolbar.middleware.DebugToolbarMiddleware',
]

# =======================================================
# django debug toolbar
INTERNAL_IPS = ['127.0.0.1']

DEBUG_TOOLBAR_PANELS = [
    'debug_toolbar.panels.versions.VersionsPanel',
    'debug_toolbar.panels.timer.TimerPanel',
    'debug_toolbar.panels.settings.SettingsPanel',
    'debug_toolbar.panels.headers.HeadersPanel',
    'debug_toolbar.panels.request.RequestPanel',
    'debug_toolbar.panels.sql.SQLPanel',
    'debug_toolbar.panels.staticfiles.StaticFilesPanel',
    'debug_toolbar.panels.templates.TemplatesPanel',
    'debug_toolbar.panels.cache.CachePanel',
    'debug_toolbar.panels.signals.SignalsPanel',
    'debug_toolbar.panels.logging.LoggingPanel',
    'debug_toolbar.panels.redirects.RedirectsPanel',
    'debug_toolbar.panels.profiling.ProfilingPanel',
]

DEBUG_TOOLBAR_CONFIG = {
    'INTERCEPT_REDIRECTS': False,
}
# =======================================================

DATABASES['default'].update({
    'NAME': 'mymdb',
    'USER': 'mymdb',
    'PASSWORD': '0000',
    'HOST': 'localhost',
    'PORT': '5432',
})

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
        'LOCATION': 'default-locmemcache',
        'TIMEOUT': 5,
    }
}

# File uploads
MEDIA_ROOT = os.path.join(BASE_DIR, '../media_root')



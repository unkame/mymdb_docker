from django.contrib.auth.forms import (
    UserCreationForm,
)
from django.urls import (
    reverse_lazy,
)
from django.views.generic import (
    CreateView,
)


## CreateView: GET --> render template
##             POST -> call form.is_valid() or form.is_invalid --> valid then save data
class RegisterView(CreateView):
    template_name = 'user/register.html'
    form_class = UserCreationForm
    success_url = reverse_lazy('core:MovieList')

-r requirements.common.txt
django-storages==1.7.2
boto3==1.10.44
uwsgi==2.0.15
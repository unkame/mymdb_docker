# App MyMdb (docker)
From the book "Building-Django-2.0-Web-Applications" by Tom Aratyn

## Description
Codes of sample app "MyMdb" from chapter 05, which describes the delpoyment of MyMdb on cloud (AWS)
You will need to register:

- an account on AWS, using its free-tier services (RDS, docker-machine, EC2). Set AWS access keys

- an account on docker hub (and install docker)

and by the book, you will:
- configure the app into production/development settings

- deploy the app MyMdb in docker container. This step is to create a DockerFile with other scripts, e.g. uwsgi, ngnix. Lots of environmental variables will be used in the deployment

- build a database container

- launch the app on AWS cloud EC2

## Others
- I put terminal's commands used at /scripts_ref/terminal_cmd for copy & paste

- .env file is not included in this repository. User shall create it own .env file


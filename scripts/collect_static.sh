#!/usr/bin/env bash

root=$1
source $root/venv/bin/activate

export DJANGO_CACHE_TIMEOUT=100
export DJANGO_SECRET_KEY="4&5&varl@0v=*4n11i7y@ajf@hc1_)rw94pm9ki&rboyk$ix7^"
export DJANGO_SETTINGS_MODULE=config.production_settings
export DJANGO_LOG_FILE=/var/log/mymdb/mymdb.log
cd $root/django/

python3 manage.py collectstatic
